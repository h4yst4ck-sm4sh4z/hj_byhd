#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gdb

class CommandDumpBYHD(gdb.Command):
    def __init__(self):
        gdb.Command.__init__(self, 'byhd', gdb.COMMAND_STACK)
        self.arch_size = gdb.lookup_type('long').pointer().sizeof
        self._arch_format = "0{0}X".format(self.arch_size * 2)
        self.p_long = gdb.lookup_type('long').pointer()
        self.p_char = gdb.lookup_type('unsigned char').pointer()

        self.decode = []
        for i in range(0x100):
            self.decode.append(None)


    def get_left(self, addr, path):
        path += "0 "
        return long(gdb.Value(addr).cast(self.p_long).dereference()), path      

    def get_right(self, addr, path):
        path += "1 "
        return long(gdb.Value(addr+8).cast(self.p_long).dereference()), path

    def leaf_value(self, addr):
        return long(gdb.Value(addr+0x10).cast(self.p_char).dereference())


    def dump_tree(self, addr, path):
        left, lpath = self.get_left(addr, path)
        right, rpath = self.get_right(addr, path)

        if left:
            self.dump_tree(left, lpath)

        else:
            if self.decode[self.leaf_value(addr)] is None:
                self.decode[self.leaf_value(addr)] = lpath

        if right:
            self.dump_tree(right, rpath)
        else:
            if self.decode[self.leaf_value(addr)] is None:
                self.decode[self.leaf_value(addr)] = rpath

    def invoke(self, arg, from_tty):
        arguments = arg.split(' ')

        r = arguments[0]
        addr = long(gdb.parse_and_eval(r))

        self.dump_tree(addr, "")

        for i in range(len(self.decode)):
            print("a[0x{0:02x}] = '{1}'".format(i, self.decode[i]))

CommandDumpBYHD()