
int64_t g1;

void func_0x401020();

int64_t func_0x400e80() {
    int64_t rax1;

    rax1 = g1;
    if (rax1 != 0) {
        func_0x401020();
    }
    return rax1;
}

int64_t g2;

void func_0x401020() {
    goto g2;
}

uint64_t func_0x4011c0() {
    uint64_t rax1;
    int64_t rax2;
    int64_t rax3;
    int64_t rax4;

    rax1 = (uint64_t)(rax2 - 0x6041a8);
    if (rax1 <= 14 || rax3 == 0) {
        return rax1;
    } else {
        goto rax4;
    }
}

struct s0 {
    int64_t f0;
    int64_t f8;
    signed char[4] pad20;
    int32_t f20;
    int32_t f24;
};

int32_t func_0x401377(struct s0* rdi, int32_t esi, int32_t* rdx) {
    struct s0* v1;
    int32_t* v2;
    signed char v3;
    signed char al4;
    int32_t ebx5;
    int32_t eax6;
    int32_t eax7;
    signed char al8;

    v1 = rdi;
    v2 = rdx;
    v3 = al4;
    ebx5 = -1;
    if (v1 != (struct s0*)0 && v2 != (int32_t*)0) {
        if (v1->f0 != 0) {
            eax6 = func_0x401377(v1->f0, (uint32_t)(unsigned char)v3, v2);
            ebx5 = eax6;
            if (ebx5 == -1) {
                eax7 = func_0x401377(v1->f8, (uint32_t)(unsigned char)v3, v2);
                ebx5 = eax7;
            }
        } else {
            if (al8 == v3) {
                *v2 = v1->f24;
                ebx5 = v1->f20;
            }
        }
    }
    return ebx5;
}

int64_t g3;

void malloc(int64_t rdi) {
    goto g3;
}

int64_t g4;

void memset(int64_t rdi, int32_t esi, int32_t edx, int64_t rcx) {
    goto g4;
}

int64_t g5;

void free(int64_t rdi, int32_t esi, int32_t edx, int64_t rcx) {
    goto g5;
}

struct s1 {
    int64_t f0;
    int64_t f8;
};

int32_t func_0x4017ca(struct s1* rdi, int32_t esi, int32_t edx) {
    struct s1* v1;
    int32_t v2;
    int32_t v3;
    int32_t v4;
    int32_t eax5;
    int64_t rax6;
    int32_t eax7;
    int64_t rax8;
    int64_t rax9;
    int32_t eax10;

    v1 = rdi;
    v2 = esi;
    v3 = edx;
    v4 = -1;
    if (v1 == (struct s1*)0) {
    } else {
        if (v2 != 0) {
            if (v1->f0 == 0) {
            } else {
                eax5 = v3;
                eax7 = func_0x4017ca(v1->f8, (int32_t)(rax6 + -1), (uint32_t)(eax5 + eax5) | 1);
                v4 = eax7;
                if (v4 == -1) {
                    eax10 = func_0x4017ca(v1->f0, (int32_t)(rax8 + -1), (int32_t)rax9);
                    v4 = eax10;
                }
            }
        } else {
            v4 = v3;
        }
    }
    return v4;
}

int64_t g6;

void func_0x402cf0(int64_t rdi, int64_t rsi, int64_t rdx) {
    goto g6;
}

int64_t g7;

void open(int64_t rdi, int32_t esi, int64_t rdx) {
    goto g7;
}

int64_t g8;

void read(int32_t edi, int64_t rsi, int32_t edx, int64_t rcx) {
    goto g8;
}

int64_t g9;

void write(int32_t edi, int64_t rsi, int32_t edx, int64_t rcx) {
    goto g9;
}

int64_t g10;

void close(int32_t edi, int64_t rsi, int32_t edx, int64_t rcx) {
    goto g10;
}

struct s2 {
    int64_t f0;
    int64_t f8;
};

struct s3 {
    signed char[16] pad16;
    signed char f16;
};

struct s4 {
    signed char[28] pad28;
    int32_t f28;
};

struct s5 {
    signed char[28] pad28;
    uint32_t f28;
};

struct s5* func_0x401df1(struct s5** rdi);

struct s6 {
    signed char[28] pad28;
    uint32_t f28;
};

struct s7 {
    struct s6* f0;
    struct s6* f8;
    signed char[12] pad28;
    uint32_t f28;
};

uint32_t func_0x401ede(void* rdi, int64_t rsi);

int64_t func_0x401f6b(void* rdi) {
    void* v1;
    int64_t rdi2;
    struct s2* rax3;
    struct s2* v4;
    int64_t rcx5;
    uint32_t ebx6;
    int64_t rdi7;
    int64_t rax8;
    int64_t rax9;
    int64_t rax10;
    int64_t rax11;
    int64_t rcx12;
    int64_t rax13;
    signed char dl14;
    int64_t rdx15;
    int64_t rax16;
    int64_t rax17;
    struct s6* rax18;
    struct s6* v19;
    struct s6* rax20;
    struct s6* v21;
    int64_t rdi22;
    struct s7* rax23;
    struct s7* v24;
    int32_t eax25;
    uint32_t ebx26;
    int64_t rax27;
    int64_t rax28;
    int32_t edx29;
    int64_t rax30;

    v1 = rdi;
    rax3 = (struct s2*)malloc(rdi2);
    v4 = rax3;
    if (v4 != (struct s2*)0) {
        memset(v4, 0, 0x808, rcx5);
        if (v1 == (void*)0) {
        } else {
            ebx6 = 0;
            while (ebx6 <= 0xff) {
                rax8 = (int64_t)malloc(rdi7);
                *(int64_t*)(rax9 * 8 + 0) = rax8;
                if (*(int64_t*)((int64_t)v4 + (rax10 * 8 + 0)) == 0) 
                    goto addr_0x402022_7;
                memset(*(int64_t*)((int64_t)v4 + (rax11 * 8 + 0)), 0, 32, rcx12);
                (*(struct s3**)((int64_t)v4 + (rax13 * 8 + 0)))->f16 = dl14;
                rcx12 = rdx15 * 4 + 0;
                (*(struct s4**)((int64_t)v4 + (rax16 * 8 + 0)))->f28 = *(int32_t*)((int64_t)v1 + rcx12);
                ++ebx6;
            }
            goto addr_0x4020f8_9;
        }
    } else {
        goto addr_0x40222a_11;
    }
    addr_0x402223_12:
    rax17 = v4->f0;
    addr_0x40222a_11:
    return rax17;
    addr_0x4020f8_9:
    memset(v1, 0, 0x400, rcx12);
    free(v1, 0, 0x400, rcx12);
    while (v4->f8 != 0) {
        rax18 = (struct s6*)func_0x401df1(v4);
        v19 = rax18;
        rax20 = (struct s6*)func_0x401df1(v4);
        v21 = rax20;
        if (v19 == (struct s6*)0) 
            goto addr_0x402155_15;
        if (v21 == (struct s6*)0) 
            goto addr_0x402155_15;
        rax23 = (struct s7*)malloc(rdi22);
        v24 = rax23;
        if (v24 == (struct s7*)0) 
            goto addr_0x40217a_18;
        memset(v24, 0, 32, rcx12);
        if (v19->f28 >= v21->f28) {
            v24->f0 = v21;
            v24->f8 = v19;
        } else {
            v24->f0 = v19;
            v24->f8 = v21;
        }
        v24->f28 = v19->f28 + v21->f28;
        eax25 = (int32_t)func_0x401ede(v4, v24);
    }
    goto addr_0x402223_12;
    addr_0x402155_15:
    v4->f0 = 0;
    goto addr_0x402223_12;
    addr_0x40217a_18:
    v4->f0 = 0;
    goto addr_0x402223_12;
    addr_0x402022_7:
    ebx26 = ebx6 - 1;
    while (ebx26 != 0) {
        if (*(int64_t*)((int64_t)v4 + (rax27 * 8 + 0)) != 0) {
            free(*(int64_t*)((int64_t)v4 + (rax28 * 8 + 0)), 0, edx29, rcx12);
            *(int64_t*)((int64_t)v4 + (rax30 * 8 + 0)) = 0;
        }
        --ebx26;
    }
    goto addr_0x402223_12;
}

struct s8 {
    int64_t f0;
    int64_t f8;
    signed char[4] pad20;
    int32_t f20;
    int32_t f24;
};

int32_t func_0x402233(struct s8* rdi, int32_t esi, int32_t edx) {
    struct s8* v1;
    int32_t v2;
    int32_t v3;
    int64_t rax4;
    int64_t rax5;
    int32_t eax6;
    int32_t eax7;
    int64_t rax8;
    int32_t eax9;

    v1 = rdi;
    v2 = esi;
    v3 = edx;
    if (v1 == (struct s8*)0) {
    } else {
        if (v1->f0 != 0) {
            eax6 = func_0x402233(v1->f0, (int32_t)rax4, (int32_t)(rax5 + 1));
            eax7 = v2;
            eax9 = func_0x402233(v1->f8, (uint32_t)(eax7 + eax7) | 1, (int32_t)(rax8 + 1));
        } else {
            if (v1->f8 == 0) {
                v1->f20 = v2;
                v1->f24 = v3;
                goto addr_0x4022d1_7;
            } else {
                goto addr_0x4022d1_7;
            }
        }
    }
    addr_0x4022d1_7:
    return 0;
}

struct s9 {
    struct s9* f0;
    struct s9* f8;
};

struct s10 {
    signed char[8] pad8;
    uint32_t f8;
    uint32_t f12;
};

int32_t func_0x40127c(struct s9* rdi, struct s10* rsi);

void memcpy(int64_t rdi, int64_t rsi, int32_t edx, int64_t rcx);

int64_t func_0x40185a(int64_t rdi, int64_t rsi, uint32_t* rdx) {
    int64_t rbp1;
    int64_t rsp2;
    int64_t v3;
    int64_t v4;
    uint32_t* v5;
    int64_t v6;
    int32_t v7;
    uint32_t v8;
    uint32_t v9;
    int64_t rax10;
    int64_t rax11;
    int64_t rcx12;
    int32_t eax13;
    int64_t rdx14;
    signed char al15;
    int64_t rax16;
    int64_t rax17;
    int64_t v18;
    int64_t rcx19;
    uint32_t edx20;
    uint32_t edx21;

    rbp1 = rsp2 - 8;
    v3 = rdi;
    v4 = rsi;
    v5 = rdx;
    v6 = 0;
    v7 = 0;
    v8 = 0;
    if (v3 == 0 || (v4 == 0 || v5 == (uint32_t*)0)) {
        addr_0x401a0f_2:
        return v6;
    } else {
        v9 = *v5 << 2;
        rax11 = (int64_t)malloc(rax10);
        v6 = rax11;
        if (v6 == 0) {
            goto addr_0x401a0f_2;
        } else {
            memset(v6, 0, v9, rcx12);
            while (v7 != -1) {
                eax13 = func_0x40127c(v3, rbp1 + -16);
                v7 = eax13;
                if (v7 == -1) 
                    goto addr_0x401a02_8;
                *(signed char*)(rdx14 + v6) = al15;
                ++v8;
                if (v8 < v9) 
                    continue;
                v9 = v9 << 1;
                rax17 = (int64_t)malloc(rax16);
                v18 = rax17;
                if (v18 == 0) 
                    goto addr_0x401966_11;
                memset(v18, 0, v9, rcx19);
                rcx19 = v6;
                memcpy(v18, rcx19, v9 >> 1, rcx19);
                edx20 = v9 >> 1;
                memset(v6, 0, edx20, rcx19);
                free(v6, 0, edx20, rcx19);
                v6 = v18;
            }
        }
    }
    addr_0x401a03_14:
    *v5 = v8;
    goto addr_0x401a0f_2;
    addr_0x401a02_8:
    goto addr_0x401a03_14;
    addr_0x401966_11:
    edx21 = v9 >> 1;
    memset(v6, 0, edx21, rcx19);
    free(v6, 0, edx21, rcx19);
    v6 = 0;
    goto addr_0x401a0f_2;
}

int64_t g11;

void mmap(int32_t edi, int64_t rsi, int32_t edx, int32_t ecx, int32_t r8d, int32_t r9d) {
    goto g11;
}

int64_t g12;

void exit(int32_t edi, int32_t esi, int32_t edx) {
    goto g12;
}

struct s11 {
    signed char[28] pad28;
    uint32_t f28;
};

struct s5* func_0x401df1(struct s5** rdi) {
    struct s5** v1;
    struct s5* v2;
    int64_t rax3;
    int64_t rax4;
    int64_t rax5;
    int64_t rax6;
    int64_t rax7;
    int64_t rax8;

    v1 = rdi;
    if (v1 == (struct s5**)0) {
    } else {
        v2 = *v1;
        while (*(int64_t*)((int64_t)v1 + (rax3 * 8 + 0)) != 0) {
            if ((*(struct s11**)((int64_t)v1 + (rax4 * 8 + 0)))->f28 < v2->f28) {
                v2 = *(struct s5**)((int64_t)v1 + (rax5 * 8 + 0));
            }
        }
        while (*(int64_t*)((int64_t)v1 + (rax6 * 8 + 0)) != 0) {
            *(int64_t*)(rax7 * 8 + 0 + (int64_t)v1) = *(int64_t*)((int64_t)v1 + (rax8 * 8 + 0));
        }
    }
    return (struct s5*)0;
}

int64_t g13;

void func___errno_location(int32_t edi, int32_t esi, int32_t edx) {
    goto g13;
}

int64_t g14;

void printf(int32_t edi, int32_t esi, int32_t edx) {
    goto g14;
}

int64_t g15;

void setsockopt(int32_t edi, int32_t esi, int32_t edx, int64_t rcx, int32_t r8d) {
    goto g15;
}

int64_t g16;

void listen(int32_t edi, int32_t esi, int32_t edx, int32_t ecx, int32_t r8d) {
    goto g16;
}

struct s12 {
    signed char[2] pad2;
    int16_t f2;
};

void getifaddrs(int64_t rdi);

struct s13 {
    struct s13* f0;
    int64_t f8;
    signed char[8] pad24;
    struct s12* f24;
};

void strcmp(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void htons(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx);

void bind(int32_t edi, int64_t rsi, int32_t edx, int64_t rcx);

void freeifaddrs(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

int32_t func_0x40245e(struct s12* rdi, int32_t esi, int32_t edx, int32_t ecx) {
    int64_t rbp1;
    int64_t rsp2;
    struct s12* v3;
    int32_t v4;
    int32_t v5;
    int32_t eax6;
    int32_t edi7;
    int32_t esi8;
    int32_t edx9;
    int32_t* rax10;
    int32_t esi11;
    int32_t edx12;
    int32_t edx13;
    struct s13* v14;
    struct s13* v15;
    int16_t ax16;
    struct s12* rdx17;
    struct s12* rsi18;
    struct s12* rcx19;
    int32_t eax20;
    int16_t ax21;
    int16_t ax22;
    int32_t edi23;
    int32_t eax24;
    int32_t esi25;
    int32_t* rax26;
    int32_t esi27;
    int16_t ax28;
    int16_t ax29;
    int32_t edi30;
    int32_t eax31;
    int32_t esi32;
    int32_t* rax33;
    int32_t esi34;
    int32_t eax35;
    int64_t v36;
    int32_t esi37;
    int32_t edx38;

    rbp1 = rsp2 - 8;
    v3 = rdi;
    v4 = esi;
    v5 = edx;
    if (v3 != (struct s12*)0) {
        eax6 = (int32_t)getifaddrs(rbp1 + -32);
        if (eax6 != 0) {
            rax10 = (int32_t*)func___errno_location(edi7, esi8, edx9);
            esi11 = *rax10;
            printf(0x402d60, esi11, edx12);
            exit(-1, esi11, edx13);
        }
        v14 = v15;
        while (v14 != (struct s13*)0) {
            if ((uint32_t)(uint16_t)ax16 == v5 && (rdx17 = v3, rsi18 = rdx17, eax20 = (int32_t)strcmp(v14->f8, rsi18, rdx17, rcx19), eax20 == 0)) {
                if (v5 == 2) {
                    ax22 = (int16_t)htons((uint32_t)(uint16_t)ax21, rsi18, rdx17, rcx19);
                    v14->f24->f2 = ax22;
                    rcx19 = v14->f24;
                    rsi18 = rcx19;
                    edi23 = v4;
                    eax24 = (int32_t)bind(edi23, rsi18, 16, rcx19);
                    if (eax24 == 0) 
                        goto addr_0x4025f8_9;
                    rax26 = (int32_t*)func___errno_location(edi23, esi25, 16);
                    esi27 = *rax26;
                    printf(0x402d80, esi27, 16);
                    exit(-1, esi27, 16);
                }
                if (v5 != 10) 
                    goto addr_0x4025f8_9;
                ax29 = (int16_t)htons((uint32_t)(uint16_t)ax28, rsi18, rdx17, rcx19);
                v14->f24->f2 = ax29;
                rcx19 = v14->f24;
                rsi18 = rcx19;
                edi30 = v4;
                eax31 = (int32_t)bind(edi30, rsi18, 28, rcx19);
                if (eax31 == 0) 
                    goto addr_0x4025f8_9;
                rax33 = (int32_t*)func___errno_location(edi30, esi32, 28);
                esi34 = *rax33;
                printf(0x402da0, esi34, 28);
                exit(-1, esi34, 28);
            }
            v14 = v14->f0;
        }
    } else {
        eax35 = -1;
        goto addr_0x40262e_16;
    }
    addr_0x4025f9_18:
    freeifaddrs(v36, rsi18, rdx17, rcx19);
    if (v14 == (struct s13*)0) {
        printf(0x402dc0, esi37, edx38);
        eax35 = -1;
    } else {
        eax35 = 0;
    }
    addr_0x40262e_16:
    return eax35;
    addr_0x4025f8_9:
    goto addr_0x4025f9_18;
}

int64_t g17;

void strcmp(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    goto g17;
}

int64_t g18;

void htons(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx) {
    goto g18;
}

int64_t g19;

void bind(int32_t edi, int64_t rsi, int32_t edx, int64_t rcx) {
    goto g19;
}

int64_t g20;

void getpwnam(int64_t rdi) {
    goto g20;
}

struct s14 {
    int64_t f0;
    signed char[8] pad16;
    int32_t f16;
    int32_t f20;
    signed char[8] pad32;
    int64_t f32;
};

void getuid();

void getgid();

void initgroups(int64_t rdi, int32_t esi, int32_t edx);

void setresgid(int32_t edi, int32_t esi, int32_t edx, int32_t ecx);

void setresuid(int32_t edi, int32_t esi, int32_t edx, int32_t ecx);

void setgid(int32_t edi, int32_t esi, int32_t edx, int32_t ecx);

void setegid(int32_t edi, int32_t esi, int32_t edx, int32_t ecx);

void setuid(int32_t edi, int32_t esi, int32_t edx, int32_t ecx);

void seteuid(int32_t edi, int32_t esi, int32_t edx, int32_t ecx);

void getegid(int32_t edi, int32_t esi, int32_t edx, int32_t ecx);

void geteuid(int32_t edi, int32_t esi, int32_t edx, int32_t ecx);

void chdir(int64_t rdi, int32_t esi, int32_t edx, int32_t ecx);

int32_t func_drop_privs(struct s14* rdi) {
    struct s14* v1;
    int32_t eax2;
    int32_t v3;
    int32_t eax4;
    int32_t v5;
    int32_t edx6;
    int32_t esi7;
    int32_t eax8;
    int32_t edx9;
    int32_t ecx10;
    int32_t esi11;
    int32_t edi12;
    int32_t eax13;
    int32_t edx14;
    int32_t ecx15;
    int32_t esi16;
    int32_t edi17;
    int32_t eax18;
    int32_t edi19;
    int32_t eax20;
    int32_t* rax21;
    int32_t eax22;
    int32_t* rax23;
    int32_t edi24;
    int32_t* rax25;
    int32_t eax26;
    int32_t* rax27;
    int32_t edi28;
    int32_t eax29;
    int32_t eax30;
    int32_t eax31;
    int32_t edx32;
    int32_t eax33;
    int32_t eax34;
    int32_t edx35;
    int32_t eax36;
    int32_t edx37;
    int32_t eax38;
    int32_t edi39;
    int32_t* rax40;
    int32_t* rax41;
    int32_t* rax42;
    int32_t* rax43;

    v1 = rdi;
    if (v1 != (struct s14*)0) {
        eax2 = (int32_t)getuid();
        v3 = eax2;
        eax4 = (int32_t)getgid();
        v5 = eax4;
        edx6 = v1->f20;
        esi7 = edx6;
        eax8 = (int32_t)initgroups(v1->f0, esi7, edx6);
        if (eax8 == 0) {
            edx9 = v1->f20;
            ecx10 = v1->f20;
            esi11 = ecx10;
            edi12 = v1->f20;
            eax13 = (int32_t)setresgid(edi12, esi11, edx9, ecx10);
            if (eax13 == 0) {
                edx14 = v1->f16;
                ecx15 = v1->f16;
                esi16 = ecx15;
                edi17 = v1->f16;
                eax18 = (int32_t)setresuid(edi17, esi16, edx14, ecx15);
                if (eax18 == 0) {
                    if (v1->f20 == v5) 
                        goto addr_0x402a6e_6;
                    edi19 = v5;
                    eax20 = (int32_t)setgid(edi19, esi16, edx14, ecx15);
                    if (eax20 != -1) 
                        goto addr_0x402a1b_8;
                } else {
                    rax21 = (int32_t*)func___errno_location(edi17, esi16, edx14);
                    printf(0x402e48, *rax21, edx14);
                    eax22 = -1;
                    goto addr_0x402b70_10;
                }
            } else {
                rax23 = (int32_t*)func___errno_location(edi12, esi11, edx9);
                printf(0x402e28, *rax23, edx9);
                eax22 = -1;
                goto addr_0x402b70_10;
            }
        } else {
            rax25 = (int32_t*)func___errno_location(edi24, esi7, edx6);
            printf(0x402e08, *rax25, edx6);
            eax22 = -1;
            goto addr_0x402b70_10;
        }
    } else {
        eax22 = -1;
        goto addr_0x402b70_10;
    }
    edi17 = v5;
    eax26 = (int32_t)setegid(edi17, esi16, edx14, ecx15);
    if (eax26 == -1) {
        addr_0x402a6e_6:
        if (v1->f16 == v3) 
            goto addr_0x402adc_15;
    } else {
        rax27 = (int32_t*)func___errno_location(edi17, esi16, edx14);
        printf(0x402e83, *rax27, edx14);
        eax22 = -1;
        goto addr_0x402b70_10;
    }
    edi28 = v3;
    eax29 = (int32_t)setuid(edi28, esi16, edx14, ecx15);
    if (eax29 == -1) {
        edi17 = v3;
        eax30 = (int32_t)seteuid(edi17, esi16, edx14, ecx15);
        if (eax30 == -1) {
            addr_0x402adc_15:
            eax31 = (int32_t)getgid();
            edx32 = v1->f20;
            if (eax31 == edx32) {
                eax33 = (int32_t)getegid(edi17, esi16, edx32, ecx15);
                if (eax33 == v1->f20) {
                    eax34 = (int32_t)getuid();
                    edx35 = v1->f16;
                    if (eax34 == edx35) {
                        eax36 = (int32_t)geteuid(edi17, esi16, edx35, ecx15);
                        edx37 = v1->f16;
                        if (eax36 == edx37) {
                            eax38 = (int32_t)chdir(v1->f32, esi16, edx37, ecx15);
                            if (eax38 == 0) {
                                eax22 = 0;
                            } else {
                                rax40 = (int32_t*)func___errno_location(edi39, esi16, edx37);
                                printf(0x402ed9, *rax40, edx37);
                                eax22 = -1;
                            }
                        } else {
                            eax22 = -1;
                        }
                    } else {
                        eax22 = -1;
                    }
                } else {
                    eax22 = -1;
                }
            } else {
                eax22 = -1;
            }
        } else {
            rax41 = (int32_t*)func___errno_location(edi17, esi16, edx14);
            printf(0x402ebc, *rax41, edx14);
            eax22 = -1;
        }
    } else {
        rax42 = (int32_t*)func___errno_location(edi28, esi16, edx14);
        printf(0x402ea0, *rax42, edx14);
        eax22 = -1;
    }
    addr_0x402b70_10:
    return eax22;
    addr_0x402a1b_8:
    rax43 = (int32_t*)func___errno_location(edi19, esi16, edx14);
    printf(0x402e67, *rax43, edx14);
    eax22 = -1;
    goto addr_0x402b70_10;
}

int64_t g21;

void setgid(int32_t edi, int32_t esi, int32_t edx, int32_t ecx) {
    goto g21;
}

int64_t g22;

void setuid(int32_t edi, int32_t esi, int32_t edx, int32_t ecx) {
    goto g22;
}

int64_t g23;

void fork(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx) {
    goto g23;
}

int32_t func_process_incoming(int64_t rdi) {
    int64_t v1;
    int32_t esi2;
    int32_t edx3;
    int64_t rax4;
    int64_t v5;
    int32_t edi6;
    int32_t esi7;
    int32_t edx8;
    int32_t* rax9;
    int32_t esi10;
    int32_t edx11;
    int32_t edx12;
    int32_t eax13;
    int32_t edx14;

    v1 = rdi;
    if (v1 == 0) {
        exit(-1, esi2, edx3);
    }
    rax4 = (int64_t)getpwnam(v1);
    v5 = rax4;
    if (v5 == 0) {
        rax9 = (int32_t*)func___errno_location(edi6, esi7, edx8);
        esi10 = *rax9;
        printf(0x402de6, esi10, edx11);
        exit(-1, esi10, edx12);
    }
    eax13 = func_drop_privs(v5);
    if (eax13 != 0) {
        exit(-1, esi10, edx14);
    }
    return 0;
}

int64_t func_read_something(int64_t rdi, int32_t* rsi) {
    int64_t rbp1;
    int64_t rsp2;
    int64_t v3;
    int32_t* v4;
    int64_t v5;
    int64_t rdx6;
    int32_t eax7;
    int64_t v8;
    int64_t rax9;
    int32_t edx10;
    int64_t rcx11;
    int64_t v12;
    int32_t eax13;
    int32_t ebx14;
    int64_t rsi15;
    int32_t edx16;
    int64_t rcx17;
    int32_t eax18;
    int32_t edx19;
    int64_t rcx20;
    int32_t eax21;
    int32_t edx22;
    int64_t rcx23;

    rbp1 = rsp2 - 8;
    v3 = rdi;
    v4 = rsi;
    v5 = 0;
    if (v3 == 0) {
    } else {
        rdx6 = rbp1 + 0xffffffffffffff60;
        eax7 = (int32_t)func_0x402cf0(v3, rdx6, rdx6);
        if (eax7 == -1) {
        } else {
            rax9 = (int64_t)malloc(v8);
            v5 = rax9;
            if (v5 == 0) {
            } else {
                memset(v5, 0, edx10, rcx11);
                eax13 = (int32_t)open(v3, 0, v12);
                ebx14 = eax13;
                if (ebx14 != -1) {
                    rsi15 = v5;
                    eax18 = (int32_t)read(ebx14, rsi15, edx16, rcx17);
                    close(ebx14, rsi15, edx19, rcx20);
                    *v4 = eax21;
                } else {
                    free(v5, 0, edx22, rcx23);
                    v5 = 0;
                }
            }
        }
    }
    return v5;
}

void* func_process_garbage(int64_t rdi, uint32_t esi, int64_t rdx) {
    uint32_t v1;
    void* v2;
    uint32_t ebx3;
    int64_t rdi4;
    void* rax5;
    int64_t rcx6;
    int32_t* rax7;
    int64_t rax8;

    v1 = esi;
    v2 = (void*)0;
    ebx3 = 0;
    if (rdi == 0) {
    } else {
        rax5 = (void*)malloc(rdi4);
        v2 = rax5;
        if (v2 == (void*)0) {
        } else {
            memset(v2, 0, 0x400, rcx6);
            while (ebx3 < v1) {
                rax7 = (int32_t*)((int64_t)v2 + (rax8 * 4 + 0));
                *rax7 = *rax7 + 1;
                ++ebx3;
            }
        }
    }
    return v2;
}

int32_t func_0x40127c(struct s9* rdi, struct s10* rsi) {
    struct s9* v1;
    struct s10* v2;
    struct s9* v3;
    uint32_t v4;
    uint32_t v5;
    signed char al6;
    signed char cl7;

    v1 = rdi;
    v2 = rsi;
    v3 = v1;
    if (v1 == (struct s9*)0 || v2 == (struct s10*)0) {
        addr_0x401371_2:
        return -1;
    } else {
        v4 = v2->f8;
        v5 = v2->f12;
        while (v3->f0 != (struct s9*)0 && v4 < v5) {
            if (((uint32_t)((int32_t)(uint32_t)(unsigned char)al6 >> cl7) & 1) == 0) {
                v3 = v3->f0;
            } else {
                v3 = v3->f8;
            }
            ++v4;
        }
        v2->f8 = v4;
        if (v3->f0 != (struct s9*)0) 
            goto addr_0x401371_2;
    }
    goto addr_0x401371_2;
}

int64_t g24;

void memcpy(int64_t rdi, int64_t rsi, int32_t edx, int64_t rcx) {
    goto g24;
}

int64_t g25;

void munmap(int64_t rdi, int64_t rsi, int32_t edx, int64_t rcx, int32_t r8d, int32_t r9d) {
    goto g25;
}

uint32_t func_0x401ede(void* rdi, int64_t rsi) {
    void* v1;
    int64_t v2;
    uint32_t ebx3;
    int64_t rax4;
    int64_t rax5;
    int64_t rax6;

    v1 = rdi;
    v2 = rsi;
    ebx3 = 0;
    if (v1 == (void*)0 || v2 == 0) {
        addr_0x401f66_2:
        return ebx3;
    } else {
        while (*(int64_t*)((int64_t)v1 + (rax4 * 8 + 0)) != 0 && ebx3 <= 0x100) {
            ++ebx3;
        }
        if (ebx3 > 0x100) 
            goto addr_0x401f2c_7;
    }
    *(int64_t*)(rax5 * 8 + 0 + (int64_t)v1) = v2;
    *(int64_t*)((int64_t)v1 + (rax6 * 8 + 0)) = 0;
    goto addr_0x401f66_2;
    addr_0x401f2c_7:
    ebx3 = 0;
    goto addr_0x401f66_2;
}

int64_t g26;

void getifaddrs(int64_t rdi) {
    goto g26;
}

int64_t g27;

void freeifaddrs(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    goto g27;
}

void send(int32_t edi, int64_t rsi, int64_t rdx, int32_t ecx);

int32_t func_0x40267e(int32_t edi, int64_t rsi, int32_t edx) {
    int32_t v1;
    int32_t v2;
    int32_t v3;
    int32_t eax4;
    int32_t eax5;

    v1 = edi;
    v2 = edx;
    v3 = 0;
    if (rsi != 0) {
        if (v2 != 0) {
            do {
                if (v3 >= v2) 
                    break;
                eax4 = (int32_t)send(v1, (int64_t)v3, (int64_t)(v2 - v3), 0);
                v3 = v3 + eax4;
            } while (v3 >= 0);
            goto addr_0x4026ee_5;
        } else {
            eax5 = 0;
            goto addr_0x402700_7;
        }
    } else {
        eax5 = -1;
        goto addr_0x402700_7;
    }
    eax5 = v3;
    addr_0x402700_7:
    return eax5;
    addr_0x4026ee_5:
    eax5 = -1;
    goto addr_0x402700_7;
}

int64_t g28;

void strlen(int64_t rdi) {
    goto g28;
}

int64_t g29;

void send(int32_t edi, int64_t rsi, int64_t rdx, int32_t ecx) {
    goto g29;
}

int64_t g30;

void recv(int32_t edi, int64_t rsi, int32_t edx, int32_t ecx) {
    goto g30;
}

void accept(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx);

void func_handle_incoming(int32_t edi, int64_t rsi, int64_t rdx) {
    int64_t rbp1;
    int64_t rsp2;
    int32_t v3;
    int64_t v4;
    int64_t v5;
    int64_t rcx6;
    int64_t rcx7;
    int64_t rdx8;
    int64_t rsi9;
    int32_t edi10;
    int32_t eax11;
    int32_t v12;
    int32_t eax13;
    int32_t v14;
    int32_t edx15;
    int32_t edx16;
    int32_t eax17;
    int32_t edx18;
    int32_t eax19;
    int32_t edi20;
    int32_t esi21;
    int32_t r15d22;
    int64_t r14_23;
    int64_t r13_24;
    int64_t rdx25;
    int64_t rax26;
    int64_t rbx27;

    rbp1 = rsp2 - 8;
    v3 = edi;
    v4 = rsi;
    v5 = rdx;
    memset(rbp1 + -48, 0, 28, rcx6);
    while (1) {
        rcx7 = rbp1 + -48;
        rdx8 = rbp1 + -64;
        rsi9 = rcx7;
        edi10 = v3;
        eax11 = (int32_t)accept(edi10, rsi9, rdx8, rcx7);
        v12 = eax11;
        if (v12 != -1) {
            eax13 = (int32_t)fork(edi10, rsi9, rdx8, rcx7);
            v14 = eax13;
            if (v14 == 0) {
                if (v14 == 0) 
                    break;
            } else {
                close(v12, rsi9, edx15, rcx7);
            }
        }
    }
    close(v3, rsi9, edx16, rcx7);
    eax17 = func_process_incoming(v5);
    edx18 = v12;
    /* i think this ends up calling process_packet() */
    eax19 = (int32_t)v4(edx18, rsi9, edx18, rcx7);
    close(v12, rsi9, edx18, rcx7);
    edi20 = eax19;
    exit(edi20, esi21, edx18);
    r15d22 = edi20;
    r14_23 = rsi9;
    r13_24 = rdx25;
    rax26 = func_0x400e80();
    if (!0) {
        do {
            *(int64_t*)(0x603df9 + rbx27 * 8)(r15d22, r14_23, r13_24, rcx7);
            ++rbx27;
        } while (rbx27 != 1);
    }
    return;
}

int64_t g31;

void getuid() {
    goto g31;
}

int64_t g32;

void getgid() {
    goto g32;
}

int64_t g33;

void initgroups(int64_t rdi, int32_t esi, int32_t edx) {
    goto g33;
}

int64_t g34;

void setresgid(int32_t edi, int32_t esi, int32_t edx, int32_t ecx) {
    goto g34;
}

int64_t g35;

void setresuid(int32_t edi, int32_t esi, int32_t edx, int32_t ecx) {
    goto g35;
}

int64_t g36;

void setegid(int32_t edi, int32_t esi, int32_t edx, int32_t ecx) {
    goto g36;
}

int64_t g37;

void seteuid(int32_t edi, int32_t esi, int32_t edx, int32_t ecx) {
    goto g37;
}

int64_t g38;

void getegid(int32_t edi, int32_t esi, int32_t edx, int32_t ecx) {
    goto g38;
}

int64_t g39;

void geteuid(int32_t edi, int32_t esi, int32_t edx, int32_t ecx) {
    goto g39;
}

int64_t g40;

void chdir(int64_t rdi, int32_t esi, int32_t edx, int32_t ecx) {
    goto g40;
}

int64_t g41;

void accept(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx) {
    goto g41;
}

int64_t g42;

void func_0x400fd0(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9, int64_t arg7, int64_t arg8) {
    goto g42;
}

void time(int32_t edi);

void srand(int32_t edi);

void signal(int32_t edi, int32_t esi);

void socket(int32_t edi, int32_t esi, int32_t edx);

int32_t func_setup_socket(int32_t edi, int32_t esi, int64_t rdx) {
    int64_t rbp1;
    int64_t rsp2;
    int32_t v3;
    int32_t v4;
    int64_t v5;
    int32_t eax6;
    int32_t edi7;
    int32_t eax8;
    int32_t v9;
    int32_t* rax10;
    int32_t esi11;
    int32_t edi12;
    int32_t eax13;
    int32_t* rax14;
    int32_t esi15;
    int32_t ecx16;
    int32_t edx17;
    int32_t esi18;
    int32_t eax19;
    int32_t eax20;

    rbp1 = rsp2 - 8;
    v3 = edi;
    v4 = esi;
    v5 = rdx;
    eax6 = (int32_t)time(0);
    srand(eax6);
    signal(17, 1);
    edi7 = v4;
    eax8 = (int32_t)socket(edi7, 1, 0);
    v9 = eax8;
    if (v9 == -1) {
        rax10 = (int32_t*)func___errno_location(edi7, 1, 0);
        esi11 = *rax10;
        printf(0x402d20, esi11, 0);
        exit(-1, esi11, 0);
    }
    edi12 = v9;
    eax13 = (int32_t)setsockopt(edi12, 1, 2, rbp1 + -8, 4);
    if (eax13 == -1) {
        rax14 = (int32_t*)func___errno_location(edi12, 1, 2);
        esi15 = *rax14;
        printf(0x402d40, esi15, 2);
        exit(-1, esi15, 2);
    }
    ecx16 = v3;
    edx17 = v4;
    esi18 = v9;
    eax19 = func_0x40245e(v5, esi18, edx17, ecx16);
    if (eax19 != 0) {
        exit(-1, esi18, edx17);
    }
    eax20 = (int32_t)listen(v9, 20, edx17, ecx16, 4);
    if (eax20 != 0) {
        exit(-1, 20, edx17);
    }
    return v9;
}

int64_t g43;

void wait4(int32_t edi, int64_t rsi, int32_t edx, int32_t ecx) {
    goto g43;
}

int64_t g44;

void signal(int32_t edi, int32_t esi) {
    goto g44;
}

int64_t g45;

void socket(int32_t edi, int32_t esi, int32_t edx) {
    goto g45;
}

int64_t g46;

void time(int32_t edi) {
    goto g46;
}

int64_t g47;

void srand(int32_t edi) {
    goto g47;
}

uint64_t func_0x4011e9() {
    uint64_t rax1;
    int64_t rax2;
    uint64_t rax3;
    int64_t rdx4;
    int64_t rdx5;

    rax1 = (uint64_t)(rax2 - 0x6041a8 >> 3);
    rax3 = rax1 + (rax1 >> 63);
    if ((int64_t)rax3 >> 1 == 0 || rdx4 == 0) {
        return rax3;
    } else {
        goto rdx5;
    }
}

signed char g48;

void func_0x401229() {
    int64_t rax1;

    if (g48 == 0) {
        rax1 = (int64_t)func_0x4011c0();
        g48 = 1;
    }
    return;
}

int64_t g49;

void func_0x40124c() {
    int64_t rax1;
    int64_t rax2;

    if (g49 == 0 || rax1 == 0) {
        goto 0x4011f0;
    } else {
        rax2(0x603e10);
        goto 0x4011f0;
    }
}

void func_0x402cd9() {
    return;
}

int64_t g50;

void func_0x401026() {
    goto g50;
}

int64_t func_0x401417(int64_t rdi, int64_t rsi, uint32_t* rdx, int64_t rcx, signed char cl) {
    int64_t rbp1;
    int64_t rsp2;
    int64_t v3;
    int64_t v4;
    uint32_t* v5;
    int64_t v6;
    uint32_t r13d7;
    uint32_t v8;
    int32_t v9;
    int64_t rax10;
    int64_t rax11;
    uint32_t eax12;
    int64_t rcx13;
    signed char al14;
    int32_t eax15;
    int64_t rdx16;
    signed char al17;
    uint32_t r12d18;
    int32_t eax19;
    int64_t rdx20;
    signed char al21;
    uint32_t eax22;
    uint32_t edx23;
    int64_t rcx24;
    int64_t rcx25;

    rbp1 = rsp2 - 8;
    v3 = rdi;
    v4 = rsi;
    v5 = rdx;
    v6 = 0;
    r13d7 = 0;
    v8 = 0;
    v9 = 0;
    if (v3 == 0 || (v4 == 0 || v5 == (uint32_t*)0)) {
        addr_0x401617_2:
        return v6;
    } else {
        rax11 = (int64_t)malloc(rax10);
        v6 = rax11;
        if (v6 == 0) {
            goto addr_0x401617_2;
        } else {
            eax12 = *v5;
            memset(v6, 0, eax12 + eax12, rcx13);
            while (*v5 > r13d7) {
                eax15 = func_0x401377(v3, (uint32_t)(unsigned char)al14, rbp1 + -52);
                if (eax15 == -1) 
                    goto addr_0x4014eb_8;
                while (v9 != 0) {
                    *(signed char*)(rdx16 + v6) = al17;
                    ++v8;
                    --v9;
                }
                ++r13d7;
            }
        }
    }
    r12d18 = v8 & 7;
    if (r12d18 != 0) {
        eax19 = func_0x4017ca(v3, 8 - r12d18, 0);
        *(signed char*)(rdx20 + v6) = al21;
    }
    *v5 = v8 + 7 >> 3;
    goto addr_0x401617_2;
    addr_0x4014eb_8:
    eax22 = *v5;
    edx23 = eax22 + eax22;
    memset(v6, 0, edx23, rcx24);
    free(v6, 0, edx23, rcx25);
    v6 = 0;
    goto addr_0x401617_2;
}

void func_0x401076() {
    goto 0x400ea0;
}

void func_0x400f96() {
    goto 0x400ea0;
}

void func_0x400eb6() {
    goto 0x400ea0;
}

void func_0x402d00() {
    return;
}

void func_0x401106() {
    goto 0x400ea0;
}

int64_t g51;

int32_t func_process_packet(int32_t edi) {
    int64_t rbp1;
    int64_t rsp2;
    int32_t v3;
    int32_t ebx4;
    int64_t rax5;
    int64_t v6;
    int64_t rcx7;
    int32_t eax8;
    int64_t rax9;
    int64_t rax10;
    int64_t v11;
    int64_t rcx12;
    int32_t eax13;
    int64_t rdx14;
    int64_t rax15;
    int64_t v16;
    int64_t rax17;
    int64_t v18;
    int32_t eax19;
    int64_t rax20;
    int64_t v21;
    int64_t rax22;
    int64_t rax23;
    int64_t v24;
    int64_t rcx25;
    int64_t rsi26;
    int64_t rdi27;
    int64_t rdx28;
    uint32_t edx29;
    int32_t esi30;
    int64_t rcx31;
    int64_t rcx32;
    int64_t rsi33;
    int32_t esi34;
    int64_t rcx35;
    int64_t rsi36;
    int64_t rcx37;
    int64_t rsi38;

    rbp1 = rsp2 - 8;
    v3 = edi;
    ebx4 = 0;
    /* reading itself */
    rax5 = func_read_something(g51, rbp1 + -68);
    v6 = rax5;
    if (v6 == 0) {
    } else {
        rcx7 = rbp1 + -76;
        eax8 = (int32_t)read(v3, rcx7, 4, rcx7);
        ebx4 = eax8;
        if ((uint1_t)(ebx4 == 0) || (uint1_t)!(int1_t)("intrinsic"() == 0)) {
        } else {
            /* this bit is actually wrong, it checks that the int received is less or equal to 255 i.e. 0xff000000*/
            if (1) {
                rax10 = (int64_t)malloc(rax9);
                v11 = rax10;
                if (v11 != 0) {
                    memset(v11, 0, 0, rcx7);
                    rcx12 = v11;
                    eax13 = (int32_t)read(v3, rcx12, 0, rcx12);
                    ebx4 = eax13;
                    if ((uint1_t)(ebx4 != 0) || (uint1_t)("intrinsic"() == 0)) {
                        /* we seem to be passing in some bytes from the binary here */
                        rax15 = (int64_t)func_process_garbage(v6, 0, rdx14);
                        v16 = rax15;
                        if (v16 == 0) {
                        } else {
                            memset(v6, 0, 0, rcx12);
                            free(v6, 0, 0, rcx12);
                            rax17 = func_0x401f6b(v16);
                            v18 = rax17;
                            if (v18 == 0) {
                            } else {
                                eax19 = func_0x402233(v18, 0, 0);
                                rax20 = func_0x40185a(v18, v11, rbp1 + -72);
                                v21 = rax20;
                                if (v21 == 0) {
                                } else {
                                    rax23 = (int64_t)mmap(0, rax22, 7, 34, -1, 0);
                                    v24 = rax23;
                                    if (v24 != 0) {
                                        rcx25 = v21;
                                        rsi26 = rcx25;
                                        rdi27 = v24;
                                        memcpy(rdi27, rsi26, 0, rcx25);
                                        v24(rdi27, rsi26, 0, rcx25, -1, 0);
                                        munmap(v24, rdx28, edx29 & 0xfffff000, rcx25, -1, 0);
                                        ebx4 = 1;
                                    } else {
                                        free(v21, esi30, 7, rcx31);
                                        rcx32 = rbp1 + -76;
                                        rsi33 = rcx32;
                                        write(v3, rsi33, 4, rcx32);
                                        close(v3, rsi33, 4, rcx32);
                                    }
                                }
                            }
                        }
                    } else {
                        free(v11, esi34, 0, rcx12);
                    }
                } else {
                    rcx35 = rbp1 + -76;
                    rsi36 = rcx35;
                    write(v3, rsi36, 4, rcx35);
                    close(v3, rsi36, 4, rcx35);
                }
            } else {
                rcx37 = rbp1 + -76;
                rsi38 = rcx37;
                write(v3, rsi38, 4, rcx37);
                close(v3, rsi38, 4, rcx37);
            }
        }
    }
    return ebx4;
}

void func_0x400fc6() {
    goto 0x400ea0;
}

void func_0x400ef6() {
    goto 0x400ea0;
}

void func_0x400fb6() {
    goto 0x400ea0;
}

int32_t func_0x401ce3(void* rdi) {
    void* v1;
    int32_t v2;
    signed char v3;
    signed char al4;
    int32_t v5;
    signed char al6;
    int32_t esi7;
    int32_t edx8;

    v1 = rdi;
    v2 = 0;
    while (*(int64_t*)((int64_t)v1 + (v2 * 8 + 0)) != 0) {
        if (**(int64_t**)((int64_t)v1 + (v2 * 8 + 0)) == 0) {
            v3 = al4;
            v5 = v2 + 1;
            while (*(int64_t*)((int64_t)v1 + (v5 * 8 + 0)) != 0) {
                if (**(int64_t**)((int64_t)v1 + (v5 * 8 + 0)) == 0) {
                    if (al6 == v3) {
                        exit(0, esi7, edx8);
                    }
                    ++v5;
                } else {
                    ++v5;
                }
            }
            ++v2;
        } else {
            ++v2;
        }
    }
    return 1;
}

void func_0x400f46() {
    goto 0x400ea0;
}

void func_0x401126() {
    goto 0x400ea0;
}

int32_t g52;

int64_t g53;

int32_t func_0x4022d8(int32_t edi, int64_t* rsi) {
    int64_t rdx1;
    int32_t eax2;
    int64_t rsi3;

    g51 = *rsi;
    eax2 = func_setup_socket(g52, 2, rdx1);
    func_handle_incoming(eax2, rsi3, g53);
    return 0;
}

void func_0x400ed6() {
    goto 0x400ea0;
}

void func_0x400f86() {
    goto 0x400ea0;
}

void func_0x400ee6() {
    goto 0x400ea0;
}

void func_0x401096() {
    goto 0x400ea0;
}

void func_0x402431(int32_t edi) {
    int64_t rbp1;
    int64_t rsp2;
    int32_t eax3;

    rbp1 = rsp2 - 8;
    do {
        eax3 = (int32_t)wait4(-1, rbp1 + -4, 1, 0);
    } while ((uint1_t)(eax3 != 0) || (uint1_t)("intrinsic"() == 0));
    return;
}

int32_t func_0x402635(int32_t edi, int64_t rsi) {
    int32_t v1;
    int64_t v2;
    int32_t eax3;
    int32_t eax4;

    v1 = edi;
    v2 = rsi;
    if (v2 != 0) {
        eax3 = (int32_t)strlen(v2);
        eax4 = func_0x40267e(v1, v2, eax3);
    } else {
        eax4 = -1;
    }
    return eax4;
}

void func_0x400ff6() {
    goto 0x400ea0;
}

void func_0x400f66() {
    goto 0x400ea0;
}

void func_0x4010d6() {
    goto 0x400ea0;
}

int32_t func_0x402702(int32_t edi, int64_t rsi, int32_t edx, int32_t ecx) {
    int64_t rbp1;
    int64_t rsp2;
    int32_t v3;
    int64_t v4;
    int32_t v5;
    signed char v6;
    signed char al7;
    int32_t v8;
    int32_t eax9;
    signed char al10;
    signed char al11;
    signed char al12;
    int32_t eax13;

    rbp1 = rsp2 - 8;
    v3 = edi;
    v4 = rsi;
    v5 = edx;
    v6 = al7;
    v8 = 0;
    if (v4 != 0) {
        if (v5 != 0) {
            do {
                eax9 = (int32_t)recv(v3, rbp1 + -9, 1, 0);
                if (eax9 <= 0) 
                    break;
                if (al10 == v6) 
                    goto addr_0x4027a1_5;
                *(signed char*)(v8 + v4) = al11;
                ++v8;
                if (v8 >= v5) 
                    goto addr_0x4027a2_7;
            } while (al12 != v6);
            goto addr_0x40279f_9;
        } else {
            eax13 = 0;
            goto addr_0x4027a5_11;
        }
    } else {
        eax13 = -1;
        goto addr_0x4027a5_11;
    }
    eax13 = -1;
    addr_0x4027a5_11:
    return eax13;
    addr_0x4027a1_5:
    addr_0x4027a2_7:
    eax13 = v8;
    goto addr_0x4027a5_11;
    addr_0x40279f_9:
    goto addr_0x4027a2_7;
}

int32_t func_0x4027a7(int32_t edi, int64_t rsi, int32_t edx) {
    int32_t v1;
    int64_t v2;
    int32_t v3;
    int32_t edx4;
    int32_t eax5;
    int32_t eax6;

    v1 = edi;
    v2 = rsi;
    v3 = edx;
    if (v2 != 0) {
        if (v3 != 0) {
            eax5 = (int32_t)recv(v1, v2, edx4, 0);
            eax6 = eax5;
        } else {
            eax6 = 0;
        }
    } else {
        eax6 = -1;
    }
    return eax6;
}

int32_t func_0x4027fc(int32_t edi, int64_t rsi, int32_t edx) {
    int32_t v1;
    int32_t v2;
    int32_t v3;
    int32_t v4;
    int32_t eax5;
    int32_t eax6;

    v1 = edi;
    v2 = edx;
    v3 = -1;
    v4 = 0;
    if (rsi != 0) {
        if (v2 != 0) {
            while (v4 < v2 && v3 != 0) {
                eax5 = (int32_t)recv(v1, (int64_t)v4, 1, 0);
                v3 = eax5;
                if (v3 <= 0) 
                    goto addr_0x402862_5;
                ++v4;
            }
        } else {
            eax6 = 0;
            goto addr_0x40287e_8;
        }
    } else {
        eax6 = -1;
        goto addr_0x40287e_8;
    }
    eax6 = v4;
    addr_0x40287e_8:
    return eax6;
    addr_0x402862_5:
    eax6 = -1;
    goto addr_0x40287e_8;
}

void func_0x401016() {
    goto 0x400ea0;
}

void func_0x4010f6() {
    goto 0x400ea0;
}

void func_0x401136() {
    goto 0x400ea0;
}

void func_0x401166() {
    goto 0x400ea0;
}

void func_0x401066() {
    goto 0x400ea0;
}

void func_0x401036() {
    goto 0x400ea0;
}

void func_0x4010b6() {
    goto 0x400ea0;
}

void func_0x401086() {
    goto 0x400ea0;
}

void func_0x4010c6() {
    goto 0x400ea0;
}

void func_0x400f16() {
    goto 0x400ea0;
}

void func_0x400f76() {
    goto 0x400ea0;
}

void func_0x400ec6() {
    goto 0x400ea0;
}

void func_0x400f36() {
    goto 0x400ea0;
}

void func_0x401056() {
    goto 0x400ea0;
}

void func_0x401146() {
    goto 0x400ea0;
}

void func_0x400f56() {
    goto 0x400ea0;
}

void func_0x400f06() {
    goto 0x400ea0;
}

void func_0x401156() {
    goto 0x400ea0;
}

void func_0x401186() {
    goto 0x400ea0;
}

void func_0x4010a6() {
    goto 0x400ea0;
}

void func_0x400fa6() {
    goto 0x400ea0;
}

void func_0x400f26() {
    goto 0x400ea0;
}

void func_0x401116() {
    goto 0x400ea0;
}

void func_0x400fd6() {
    goto 0x400ea0;
}

void func_0x4010e6() {
    goto 0x400ea0;
}

void func_0x401006() {
    goto 0x400ea0;
}

void func_0x401190() {
    void* rsp1;
    int64_t* rsp2;
    int64_t v3;
    int64_t rdx4;
    int64_t rax5;

    rsp1 = (void*)(rsp2 + 1);
    func_0x400fd0(0x4022d8, v3, rsp1, 0x402c50, 0x402ce0, rdx4, ((uint64_t)rsp1 & 0xfffffffffffffff0) - 8 - 8, rax5);
    __asm__("hlt")
}

void func_0x401176() {
    goto 0x400ea0;
}

void func_0x401046() {
    goto 0x400ea0;
}

void func_0x400fe6() {
    goto 0x400ea0;
}
